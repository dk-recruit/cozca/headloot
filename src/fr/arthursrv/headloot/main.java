package fr.arthursrv.headloot;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin implements Listener  {

	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage("Le plugin HeadLoot a d�marr�");
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	    public void onDeath(PlayerDeathEvent e) {
			Player player = e.getEntity();
	        ItemStack item = new ItemStack(Material.SKULL, 1, (short) 3);
	        SkullMeta sm = (SkullMeta) item.getItemMeta();
	        sm.setOwner((e.getEntity().getName()));
	        item.setItemMeta(sm);

	        Random random = new Random();
	        int chanceloot = random.nextInt(100);
	        
	        if(chanceloot <= 10 && player.getKiller().hasPermission("headloot.10")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 20 && player.getKiller().hasPermission("headloot.20")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 30 && player.getKiller().hasPermission("headloot.30")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 40 && player.getKiller().hasPermission("headloot.40")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 50 && player.getKiller().hasPermission("headloot.50")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 60 && player.getKiller().hasPermission("headloot.60")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 70 && player.getKiller().hasPermission("headloot.70")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 80 && player.getKiller().hasPermission("headloot.80")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 90 && player.getKiller().hasPermission("headloot.90")) {
	        	e.getDrops().add(item);
	        }
	        
	        if(chanceloot <= 100 && player.getKiller().hasPermission("headloot.100")) {
	        	e.getDrops().add(item);
	        }
	    }
	
	
	
	
}
